/*
 * Open Quiz
 * 	A simple study tool
 */


// Quiz data containers
const htmlContainer = document.getElementsByTagName("html")[0];
const quizDiv = document.getElementById("quiz");
const introDiv = document.getElementById("intro");
const questionDiv = document.getElementById("question");
const answersDiv = document.getElementById("answers");
const scoreDiv = document.getElementById("score");
const timeDiv = document.getElementById("time");
const clickToShowDiv = document.getElementById("clickToShowDiv");

// Setting inputs
const wordOrderInput = document.getElementById("wordOrder");
const gameModeInput = document.getElementById("gameMode");
const numQuestionsInput = document.getElementById("numQuestions");
const timeDefaultInput = document.getElementById("timeDefault");
const csvDataInput = document.getElementById("csvData");
const timerInput = document.getElementById("timer");
const playCSVAudioInput = document.getElementById("playCSVAudioInput");
const themeInput = document.getElementById("theme");
const clickToShowInput = document.getElementById("clickToShow");

const params = new URLSearchParams(window.location.search);

// Setting variables
// Word Order:
// This sets which word is the question
// and which one is the answer
// - term (First word in CSV),
// - definition (Second word in CSV)
let wordOrder = params.has('wordOrder') ? params.get('wordOrder') : "term";
// Game Modes: 
// - flashcard, 
// - multichoice
let gameMode = params.has('gameMode') ? params.get('gameMode') : "multichoice";
let numQuestions = params.has('numQuestions') ? params.get('numQuestions') : 8;
let timeMode = params.has('timeMode') ? params.get('timeMode') : true;
let timeDefault = params.has('timeDefault') ? params.get('timeDefault') : 120;
let clickToShow = params.has('clickToShow') ? params.get('clickToShow') : false;
let theme = params.has('theme') ? params.get('theme') : "light";
let playCSVAudio = params.has('playCSVAudio') ? params.get('playCSVAudio') : true;

let gameData = false;
let timeRunning = false;
let correctAnswer = "";

let time = timeDefault;
let hiScore = 0;
let score = 0;

// Save settings
document.getElementById("form").addEventListener("submit", (event) => {
	event.preventDefault();

	// Save game settings
	wordOrder = wordOrderInput.value;
	gameMode = gameModeInput.value;
	numQuestions = numQuestionsInput.value;
	timeDefault = timeDefaultInput.value;
	timeMode = (timerInput.value === "true") ? true : false;
	clickToShow = (clickToShowInput.value === "true") ? true : false;
	theme = themeInput.value;
	playCSVAudio = (playCSVAudioInput.value === "true") ? true : false;

	if (theme === "dark") {
		htmlContainer.classList.add("inverted");
	} else {
		htmlContainer.classList.remove("inverted");
	}

	// Reset game data
	updateScore(0);
	hiScore = 0;
	updateTime(timeDefault);

	// Run game
	readAndRun();
	location.hash = "#main";
});			

// Handle URL parameters
window.onload = () => {
	if (params.has("url")) {
		readUrl(params.get("url"));
	}
};

// Read a CSV from a URL if given
function readUrl(url) {
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
		if (xhr.readyState === 4) {
			csvDataInput.value = xhr.responseText;
			readAndRun();
		}
	};
	xhr.open("GET", url);
	xhr.send();
}

// Read CSV then run game
function readAndRun() {
	csvToData();
	if (!gameData ||
			gameData.length < 1 ||
			(gameData.length < numQuestions && 
			gameMode === "multichoice")
	) {
		alert("Not enough questions");
	} else if (gameData) {
		runGame();
	}
}

// Convert CSV data from input form
// to array of objects
function csvToData() {
	let csvData = csvDataInput.value;
	let array = csvData.split("\n");
	gameData = [];
	for (let item of array) {
		let row = item.split(",");
		if (row.length >= 2) {
			let obj = {};
			obj.term = row[0].trim();
			obj.definition = row[1].trim();
			if (row.length > 2) {
				obj.audio = row[2].trim();
			}
			gameData.push(obj);
		}
	}
}

// Timing system
const delay = ms => new Promise(res => setTimeout(res, ms));
async function timer() {
	timeRunning = true;
	await delay(1000);
	time--;
	if (timeMode) {
		if (time <= 0) {
			updateTime(timeDefault);
			let audio = new Audio("timesup.mp3");
			audio.play();
			yourScore = score;
			updateScore(0);
			alert("TIMES UP\nYour Score:" + yourScore + "\nHi-Score:" + hiScore);
			timeRunning = false;
			window.top.postMessage('done', '*')
		} else {
			updateTime(time);
			timer();
		}
	} else {
		timeRunning = false;
	}
}
function updateTime(newTime) {
	time = newTime;
	timeDiv.innerHTML = "Time: " + time;
}

// Score tracking functions attached to
// onClick events in the html answers
function correct() { // eslint-disable-line
	updateScore(score + 1);
	runGame();
}
function incorrect() { // eslint-disable-line 
	let audio = new Audio("wrong.mp3");
	audio.play();
	if (timeMode) {
		alert("Incorrect\nCorrect:" + correctAnswer);
	} else {
		alert("Incorrect\nCorrect:" + correctAnswer + "\nYour Score:" + score + "\nHi-Score:" + hiScore);
		updateScore(0);
	}
	runGame();
}
function updateScore(newScore) {
	if (!newScore && score > hiScore) {
		hiScore = score;
	} 
	if (newScore) {
		let audio = new Audio("correct.mp3");
		audio.play();
	}
	score = newScore;
	scoreDiv.innerHTML = "Score: " + score;
	if (hiScore) {
		scoreDiv.innerHTML += "/" + hiScore;
	}
	if (score > hiScore) {
		scoreDiv.style = "color:green;";
		scoreDiv.innerHTML = "&#10024 " + scoreDiv.innerHTML + " &#10024";
	} else {
		scoreDiv.style = "color:grey;";
	}
}

// Selects random number of items from question bank
// Used for multichoice quiz mode
function randomItems(i) {
	let count = 0;
	let items = [];
	let questionBank = JSON.parse(JSON.stringify(gameData));
	while (count < i && questionBank.length > 0) {
		let rand = Math.floor(Math.random() *questionBank.length);
		items.push(questionBank[rand]);
		questionBank.splice(rand, 1);
		count++;
	}
	return items;
}

// Removes and returns a single random item from question bank
// Used for flashcard mode
function getItem() {
	let items = [];
	if (gameData.length > 0) {
		items.push(gameData.splice(Math.floor(Math.random()*gameData.length), 1)[0]);
	} else {
		alert("You hit the end of your list. The list will now reset");
		// Reset question bank after end is reached
		csvToData();
	}
	return items;
}

// Show answers after click
function showAnswers() { //eslint-disable-line
	answersDiv.classList.remove("hide");
	clickToShowDiv.classList.add("hide");
}

// Main game function
function runGame() {
	// Show content
	introDiv.classList.add("hide");
	quizDiv.classList.remove("hide");

	// Handle click to show answer
	if (clickToShow) {
		clickToShowDiv.classList.remove("hide");
		answersDiv.classList.add("hide");
	} else {
		clickToShowDiv.classList.add("hide");
		answersDiv.classList.remove("hide");
	}

	// Handle timer
	if (timeMode) {
		if (!timeRunning) {
			timer();
		}
		timeDiv.classList.remove("hide");
	} else {
		timeDiv.classList.add("hide");
	}

	let items = (gameMode === "multichoice") ? 
		randomItems(numQuestions) : 
		getItem();
	populate(items);
}

// Populate screen with content
function populate(items) {
	// Select correct answer
	let correct = Math.floor(Math.random() * items.length);
	// Handle term/definition order
	if (wordOrder === "term") {
		questionDiv.innerHTML = items[correct].term;
		correctAnswer = items[correct].definition;
	} else {
		questionDiv.innerHTML = items[correct].definition;
		correctAnswer = items[correct].term;
	}

	// Clear display
	let range = document.createRange();
	range.selectNodeContents(answersDiv);
	range.deleteContents();

	// Populate answers
	for (const [index,value] of items.entries()) {
		let answer = document.createElement("div");
		answer.id = "answer";
		answer.classList = "padding-top";

		// Handle term/definition order
		if (wordOrder === "term") {
			answer.innerHTML = value.definition;
		} else {
			answer.innerHTML = value.term;
		}

		if (index === correct) {
			answer.setAttribute("onclick", "correct()");
		} else {
			answer.setAttribute("onclick", "incorrect()");
		}
		answersDiv.appendChild(answer);
	}

	// Play audio if present
	if (items[correct].audio && playCSVAudio) {
		let audio = new Audio(items[correct].audio);
		audio.play();
	}
}
