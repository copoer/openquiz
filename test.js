// 1 Minute Quiz for every 5 minutes content
//const addCSS = css => document.head.appendChild(document.createElement("style")).innerHTML=css;

let dia = `
<dialog id="reallycoolquiz" style="height:80rem;width:50rem;z-index:10000000;margin-top:5rem;filter:none !important;" autofocus>
	<iframe
	src="https://copoer.gitlab.io/quizdata/11507a0e2f5e69d5dfa40a62a1bd7b6ee57e6bcd85c67c9b8431b36fff21c437/"
	style="height:100%;width:100%;">
	</iframe>
	  <form method="dialog">
    <button>Close</button>
  </form>
</dialog>
`
// Inject into page
let div = document.createElement('div');
div.innerHTML = dia.trim();
document.body.insertBefore(div, document.body.children[0]);

function quizTime() {
	// Pause all content
	document.querySelectorAll('video').forEach(vid => vid.pause());
	document.querySelectorAll('audio').forEach(el => el.pause());
	// Display Quiz
	document.getElementById("reallycoolquiz").show();
}
quizTime();

window.onmessage = function(e) {
	if (e.data == 'done') {
		document.getElementById("reallycoolquiz").close();
		let win = Math.floor(Math.random() * 3)+1;
		const prizeDialog = document.querySelector("#prize-dialog");
		prizeDialog.show();
		const winnings = document.querySelector("#winnings");
		winnings.innerText = win;
		setTimeout(() => {
			console.log("Running again soon");
			quizTime();
		}, 1000 * 60 * win);
	}
};
