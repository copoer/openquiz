# Open Quiz

A simple vanilla JS multi choice quiz website.

The quizzes are generated from a CSV pasted in the settings or linked via a URL to a CSV in the url params.

[Live demo](https://copoer.gitlab.io/openquiz/?url=example.csv)

## TODO

- Random prize
- Record game history in cookies
- Daily quiz/punch pass

## Sound sources

Correct:
https://pixabay.com/sound-effects/?utm_source=link-attribution&utm_medium=referral&utm_campaign=music&utm_content=6776

Wrong:
https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=music&utm_content=6346

Times Up:
https://pixabay.com/sound-effects/?utm_source=link-attribution&utm_medium=referral&utm_campaign=music&utm_content=14474

## Example CSV

The format for the CSV is as follows:

`Term,Definition,URL to sound file`


Example CSV:

```csv
bā,eight
èr,two
jiǔ,nine
líng,zero
liù,six
qī,seven
sān,three
shí,ten
sì,four
wǔ,five
yī,one
bǎi,hundred
qiān,thousand
wàn,ten thousand
```

Example linking via URL:

`https://copoer.gitlab.io/openquiz/?url=example.csv`


## Screenshots

![Demo](Demo.png)
